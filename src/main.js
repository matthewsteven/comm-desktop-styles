import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import AppResizePanes from './AppResizePanes.vue'
import AppTabPanel from './AppTabPanel.vue'
import AppWindowManager from './AppWindowManager.vue'
import AppWindowManagerWindow from './AppWindowManagerWindow.vue'
import AppClaims from './AppClaims.vue'
import AppTest from './AppTest.vue'

import "bootstrap-vue/dist/bootstrap-vue.css"

Vue.use(BootstrapVue);

Vue.component('app-resize-panes', AppResizePanes);
Vue.component('app-tab-panel', AppTabPanel);
Vue.component('app-claims', AppClaims);
Vue.component('app-test', AppTest);
Vue.component('app-window-manager', AppWindowManager);
Vue.component('app-window-manager-window', AppWindowManagerWindow);

new Vue({
	el: '#app',
	render: h => h(App)
})
