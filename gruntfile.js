module.exports = function (grunt) {

	var sass = require('node-sass');
	var fs = require('fs');

	grunt.initConfig({
		webfont: {
			icons: {
				src: 'src/assets/images/font-icons/*.svg',
				dest: 'src/assets/fonts',
				options: {
					font: 'icons-custom',
					fontFilename: 'icons-custom',
					stylesheets: ['css'],
					template: 'src/assets/images/font-icons/template.css'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-webfont');

	grunt.registerTask('sass-compile', 'Compiles bootstrap sass themes.', function () {
		var done = this.async();

		var handleFileWatch = (folder, meta) => {
			return function(event, filename) {
				if (/\.scss$/.test(filename)) {
					let fullName = folder + '/' + filename;
					console.log('Compiling ' + fullName + ' ...');
					let csmStylesheet = fs.readFileSync(fullName, 'utf8');
					try {
						var result = sass.renderSync({
							data: csmStylesheet
						});
						let css = result.css.toString();
						
						css = css.replace(/\@media\s*\(min\-width\:\s*576px\)/g, '.breakpoint-min-sm');
						css = css.replace(/\@media\s*\(min\-width\:\s*768px\)/g, '.breakpoint-min-md');
						css = css.replace(/\@media\s*\(min\-width\:\s*992px\)/g, '.breakpoint-min-lg');
						css = css.replace(/\@media\s*\(min\-width\:\s*1200px\)/g, '.breakpoint-min-xl');
						css = css.replace(/\@media\s*\(max\-width\:\s*575\.98px\)/g, '.breakpoint-max-sm');
						css = css.replace(/\@media\s*\(max\-width\:\s*767\.98px\)/g, '.breakpoint-max-md');
						css = css.replace(/\@media\s*\(max\-width\:\s*991\.98px\)/g, '.breakpoint-max-lg');
						css = css.replace(/\@media\s*\(max\-width\:\s*1199\.98px\)/g, '.breakpoint-max-xl');

						result = sass.renderSync({
							data: css
						});

						fs.writeFileSync(fullName.replace('.scss', '.css'), result.css);
						console.log('Compile complete.');
					} catch(e) {
						console.warn(e);
						console.log('Compile stopped - error occurred.');
					}
					
				}
			}
		};

		console.log('Watching for changes...');
		fs.watch('src', handleFileWatch('src'));

	});

};